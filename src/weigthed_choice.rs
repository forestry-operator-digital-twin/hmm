use super::*;
/// Sample from a [categorical distribution](https://en.wikipedia.org/wiki/Categorical_distribution)
/// where the weight for each category is a float.
pub struct WeightedChoiceFloat {
    cmf: Vec<f64>,
}

impl WeightedChoiceFloat {
    pub fn from_pmf(pmf: &[f64]) -> Self {
        let cmf = pmf
            .iter()
            .scan(0.0, |state, x| {
                *state += x;
                Some(*state)
            })
            .collect();
        Self { cmf }
    }
}

impl Distribution<usize> for WeightedChoiceFloat {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> usize {
        let sampled_uniform = rng.gen::<f64>();
        let (i, _x) = self
            .cmf
            .iter()
            .enumerate()
            .find(|(_i, &x)| sampled_uniform < x)
            .unwrap();
        i
    }
}