use itertools::Itertools;
use ndarray::{ArrayBase, Data, DataMut, Dimension, Ix1, Ix2};
use num_traits::{Float, Num, Zero};

pub trait ArrayFloat<T: Float> {
    fn l2_distance(&self, rhs: &Self) -> T;
}

pub trait Array1Float<T: Float> {
    /// Along a 1D array, return the maximum float value and its index
    ///
    /// If there are multiple equal maximum values, one of them will be returned with its index.
    ///
    /// The behavior of this function is unspecified if the array contains NaNs.
    ///
    /// See also `maxfx`
    fn maxf(&self) -> Option<(usize, T)>;

    /// The "expecting" version of `maxf`
    fn maxfx(&self) -> (usize, T);
}

pub trait Array1FloatMut {
    fn nip(&mut self, label: &'static str);

    fn normalize(self, label: &'static str) -> Self;
}

pub trait Array1Num<T>
where
    T: Copy + Num,
{
    fn sum(&self) -> T;
}

pub trait Array2FloatMut {
    fn nip_rows(&mut self);

    fn normalize_rows(self) -> Self;
}

impl<D, S> ArrayFloat<f64> for ArrayBase<S, D>
where
    D: Dimension,
    S: Data<Elem = f64>,
{
    fn l2_distance(&self, rhs: &Self) -> f64 {
        assert_eq!(self.shape(), rhs.shape());
        self.iter()
            .zip(rhs.iter())
            .map(|(&x, &y)| (y - x).powi(2))
            .sum::<f64>()
            .sqrt()
    }
}

impl<T, S> Array1Float<T> for ArrayBase<S, Ix1>
where
    T: Float,
    S: Data<Elem = T>,
{
    fn maxf(&self) -> Option<(usize, T)> {
        self.iter()
            .enumerate()
            .fold1(|(i0, v0), (i1, v1)| if v0 > v1 { (i0, v0) } else { (i1, v1) })
            .map(|(i, &v)| (i, v))
    }

    fn maxfx(&self) -> (usize, T) {
        self.maxf()
            .expect("maxfx failed because the input had length 0")
    }
}

impl<S> Array1FloatMut for ArrayBase<S, Ix1>
where
    S: DataMut + Data<Elem = f64>,
{
    fn nip(&mut self, label: &'static str) {
        let sum: f64 = self.sum();
        assert!(sum.is_sign_positive(), "Sum of {} must be positive", label);
        (*self) /= sum;
    }

    fn normalize(mut self, label: &'static str) -> Self {
        self.nip(label);
        self
    }
}

impl<T, S> Array1Num<T> for ArrayBase<S, Ix1>
where
    T: Copy + Num,
    S: Data<Elem = T>,
{
    fn sum(&self) -> T {
        self.iter().fold(Zero::zero(), |v0, &v1| v0 + v1)
    }
}

impl<S> Array2FloatMut for ArrayBase<S, Ix2>
where
    S: DataMut + Data<Elem = f64>,
{
    fn nip_rows(&mut self) {
        for mut row in self.rows_mut() {
            let sum: f64 = row.sum();
            assert!(sum > Zero::zero());
            row /= sum;
        }
    }

    fn normalize_rows(mut self) -> Self {
        self.nip_rows();
        self
    }
}
