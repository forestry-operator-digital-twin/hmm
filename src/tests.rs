#[cfg(test)]
mod tests {
    use crate::util::{Array2FloatMut as _, ArrayFloat as _};
    use crate::{new_rng, HMMFilterItem, HMMSample, HMM};
    use core::iter::repeat_with;
    use counter::Counter;
    use itertools::Itertools;
    use lazy_static::lazy_static;
    use ndarray::{array, Array1, Array2};
    use rand::Rng;
    use spectral::assert_that;
    use spectral::numeric::OrderedAssertions;

    lazy_static! {
        static ref HMM_UNIT: HMM = HMM::new(array![[1.0]], array![[1.0]], array![1.0]);
    }

    lazy_static! {
        static ref HMM_PERIODIC: HMM = {
            HMM::new(
                array![[0.0, 1.0], [1.0, 0.0]],
                array![[0.0, 1.0], [1.0, 0.0]],
                array![1.0, 0.0],
            )
        };
    }

    // A hand-calculated table of paths where each path is equally likely.
    //
    // 1 time step (state only)
    // 0
    // 2
    //
    // 1 time step (state + observation)
    // 0/0
    // 2/1
    //
    // 2 time steps (states only)
    // 0, 0
    // 0, 1
    // 0, 2
    // 0, 2
    // 2, 0
    // 2, 1
    // 2, 1
    // 2, 2
    //
    // 2 time steps (states + observations)
    // 0/0, 0/0
    // 0/0, 0/0
    // 0/0, 1/0
    // 0/0, 1/1
    // 0/0, 2/1
    // 0/0, 2/1
    // 0/0, 2/1
    // 0/0, 2/1
    // 2/1, 0/0
    // 2/1, 0/0
    // 2/1, 1/0
    // 2/1, 1/0
    // 2/1, 1/1
    // 2/1, 1/1
    // 2/1, 2/1
    // 2/1, 2/1
    lazy_static! {
        static ref HMM_FANCY: HMM = {
            HMM::new(
                array![[0.25, 0.25, 0.5], [0.5, 0.25, 0.25], [0.25, 0.5, 0.25]],
                array![[1.0, 0.0], [0.5, 0.5], [0.0, 1.0]],
                array![0.5, 0.0, 0.5],
            )
        };
    }

    // 1 time step (state + observation)
    // 0/0
    // 0/0
    // 0/1
    // 0/1
    // 1/0
    // 1/0
    // 1/0
    // 1/1
    //
    // 2 time steps (states only)
    // 0, 0
    // 0, 1
    // 0, 1
    // 0, 1
    // 1, 0
    // 1, 0
    // 1, 1
    // 1, 1
    //
    // 2 time steps (states + observations)
    lazy_static! {
        static ref HMM_COOL: HMM = {
            HMM::new(
                array![[0.25, 0.75], [0.5, 0.5]],
                array![[0.5, 0.5], [0.75, 0.25]],
                array![0.5, 0.5],
            )
        };
    }

    #[test]
    fn sampler_unit() {
        let rng = &mut new_rng();
        let mut sampler = HMM_UNIT.sampler(rng);
        assert_eq!(HMMSample { x: 0, y: 0 }, sampler.next().unwrap());
        assert_eq!(HMMSample { x: 0, y: 0 }, sampler.next().unwrap());
    }

    #[test]
    fn sampler_periodic() {
        let rng = &mut new_rng();
        let mut sampler = HMM_PERIODIC.sampler(rng);
        assert_eq!(HMMSample { x: 0, y: 1 }, sampler.next().unwrap());
        assert_eq!(HMMSample { x: 1, y: 0 }, sampler.next().unwrap());
        assert_eq!(HMMSample { x: 0, y: 1 }, sampler.next().unwrap());
    }

    #[test]
    fn ll_given_states_empty() {
        assert!((0.0 - HMM_UNIT.ll_given_states(&[], &[])).abs() < f64::EPSILON)
    }

    #[test]
    fn ll_given_states_one() {
        assert!((0.0 - HMM_UNIT.ll_given_states(&[0], &[0])).abs() < f64::EPSILON)
    }

    #[test]
    fn ll_given_states_certain() {
        assert!((0.0 - HMM_PERIODIC.ll_given_states(&[0, 1, 0], &[1, 0, 1])).abs() < f64::EPSILON)
    }

    #[test]
    fn ll_given_states_impossible_initial_state() {
        let r = HMM_PERIODIC.ll_given_states(&[1], &[0]);
        assert!(r.is_infinite() & r.is_sign_negative())
    }

    #[test]
    fn ll_given_states_impossible_transition() {
        let r = HMM_PERIODIC.ll_given_states(&[0, 0], &[1, 1]);
        assert!(r.is_infinite() & r.is_sign_negative())
    }

    #[test]
    fn ll_given_states_impossible_observation() {
        let r = HMM_PERIODIC.ll_given_states(&[0], &[0]);
        assert!(r.is_infinite() & r.is_sign_negative())
    }

    #[test]
    fn filter_empty() {
        assert_eq!(
            Vec::<HMMFilterItem>::new(),
            HMM_FANCY.filter(core::iter::empty()).collect_vec()
        )
    }

    #[test]
    fn filter_zero() {
        assert_eq!(
            // array![1.0, 0.0, 0.0]
            vec![HMMFilterItem::new(array![1.0, 0.0, 0.0])],
            HMM_FANCY.filter(vec![0]).collect_vec(),
            "must be in state 0 because we can't start in state 1 and state 2 can't emit 0"
        )
    }

    #[test]
    fn filter_one() {
        assert_eq!(
            vec![HMMFilterItem::new(array![0.0, 0.0, 1.0])],
            HMM_FANCY.filter(vec![1]).collect_vec(),
            "must be in state 2 because we can't start in state 1 and state 0 can't emit 1"
        )
    }

    /// Calculated by hand using the probability table for HMM_FANCY
    #[test]
    fn filter_zero_zero() {
        assert_eq!(
            vec![
                HMMFilterItem::new(array![1.0, 0.0, 0.0]),
                HMMFilterItem::new(array![2.0 / 3.0, 1.0 / 3.0, 0.0]),
            ],
            HMM_FANCY.filter(vec![0, 0]).collect_vec(),
        )
    }

    /// Calculated by hand using the probability table for HMM_FANCY
    #[test]
    fn filter_zero_one() {
        assert_eq!(
            vec![
                HMMFilterItem::new(array![1.0, 0.0, 0.0]),
                HMMFilterItem::new(array![0.0, 1.0 / 5.0, 4.0 / 5.0]),
            ],
            HMM_FANCY.filter(vec![0, 1]).collect_vec(),
        )
    }

    /// Calculated by hand using the probability table for HMM_FANCY
    #[test]
    fn filter_one_zero() {
        assert_eq!(
            vec![
                HMMFilterItem::new(array![0.0, 0.0, 1.0]),
                HMMFilterItem::new(array![0.5, 0.5, 0.0]),
            ],
            HMM_FANCY.filter(vec![1, 0]).collect_vec(),
        )
    }

    /// Calculated by hand using the probability table for HMM_FANCY
    #[test]
    fn filter_one_one() {
        assert_eq!(
            vec![
                HMMFilterItem::new(array![0.0, 0.0, 1.0]),
                HMMFilterItem::new(array![0.0, 0.5, 0.5]),
            ],
            HMM_FANCY.filter(vec![1, 1]).collect_vec(),
        )
    }

    /// Calculated by hand by looking at the transition matrix
    #[test]
    fn predict_zero_steps() {
        assert_eq!(
            array![0.5, 0.5, 0.0],
            HMM_FANCY.predict(array![0.5, 0.5, 0.0], 0)
        )
    }

    /// Calculated by hand by looking at the transition matrix
    #[test]
    fn predict_one_step() {
        assert_eq!(
            array![0.375, 0.25, 0.375],
            HMM_FANCY.predict(array![0.5, 0.5, 0.0], 1)
        )
    }

    /// Calculated by hand by looking at the transition matrix
    #[test]
    fn predict_two_steps() {
        assert_eq!(
            array![0.3125, 0.34375, 0.34375],
            HMM_FANCY.predict(array![0.5, 0.5, 0.0], 2)
        )
    }

    /// Sample a sequence from this HMM that produces the given sequence of observations. This will
    /// only work well for few observations.
    fn sample_matching<R: Rng>(
        hmm: &HMM,
        observations: &Array1<usize>,
        rng: &mut R,
    ) -> Array1<HMMSample> {
        for _ in 0..10000 {
            let samples = hmm
                .sampler(rng)
                .take(observations.len())
                .collect::<Array1<_>>();
            let matches = observations
                .iter()
                .zip(&samples)
                .all(|(&observation, sample)| observation == sample.y);
            if matches {
                return samples;
            }
        }
        panic!("That was an unlikely sequence of observations")
    }

    /// Calculate an approximation of smoothing by sampling. Useful for testing `smooth`.
    fn smooth_sampled(hmm: &HMM, observations: &Array1<usize>, n_iterations: usize) -> Array2<f64> {
        let mut rng = new_rng();
        let mut counts = Array2::zeros((observations.len(), hmm.n()));
        for _ in 0..n_iterations {
            let samples = sample_matching(hmm, observations, &mut rng);
            for (t, sample) in samples.iter().enumerate() {
                counts[(t, sample.x)] += 1.0;
            }
        }

        counts.normalize_rows()
    }

    /// Calculate an approximation of most likely sequence by sampling. Useful for testing
    /// `most_likely_sequence`.
    fn most_likely_sequence_sampled(
        hmm: &HMM,
        observations: &Array1<usize>,
        n_iterations: usize,
    ) -> Array1<usize> {
        let mut rng = new_rng();
        let (sequence, count) = dbg!(repeat_with(|| sample_matching(hmm, observations, &mut rng))
            .take(n_iterations)
            .collect::<Counter<_>>()
            .most_common())
        .remove(0);
        assert_that(&count).is_greater_than(0);
        sequence.map(|hmm_sample| hmm_sample.x)
    }

    fn test_smooth(observations: Array1<usize>, n_iterations: usize) {
        let expected = smooth_sampled(&HMM_COOL, &observations, n_iterations);
        let actual = HMM_COOL.smooth(&observations);
        let distance = expected.l2_distance(&actual);
        assert_that(&distance).is_less_than(0.3);
    }

    #[test]
    fn test_smooth_zero() {
        test_smooth(array![0], 100);
    }

    #[test]
    fn test_smooth_one() {
        test_smooth(array![0], 100);
    }

    #[test]
    fn test_smooth_zero_zero() {
        test_smooth(array![0, 0], 100);
    }

    #[test]
    fn test_smooth_zero_one() {
        test_smooth(array![0, 1], 100);
    }

    #[test]
    fn test_smooth_zero_zero_zero() {
        test_smooth(array![0, 0, 0], 10000);
    }

    #[test]
    fn test_smooth_zero_one_one() {
        test_smooth(array![0, 1, 1], 10000);
    }

    #[test]
    fn test_train_n_1_k_1_no_observations() {
        let observations = array![];
        let hmm = HMM::train(&observations, 1, 1, &mut new_rng());
        assert_eq!(hmm.a, array![[1.0]]);
        assert_eq!(hmm.b, array![[1.0]]);
        assert_eq!(hmm.pi, array![1.0]);
    }

    #[test]
    fn test_train_n_2_k_2_no_observations() {
        let observations = array![];
        let hmm = HMM::train(&observations, 2, 2, &mut new_rng());
        assert_eq!(hmm.a, array![[0.5, 0.5], [0.5, 0.5]]);
        assert_eq!(hmm.b, array![[0.5, 0.5], [0.5, 0.5]]);
        assert_eq!(hmm.pi, array![0.5, 0.5]);
    }

    // An HMM with one state that always emits the same thing
    #[test]
    fn test_train_n_1_k_1_constant() {
        let observations = array![0, 0];
        let hmm = HMM::train(&observations, 1, 1, &mut new_rng());
        assert_eq!(hmm.a, array![[1.0]]);
        assert_eq!(hmm.b, array![[1.0]]);
        assert_eq!(hmm.pi, array![1.0]);
    }

    // An HMM with one state that always emits the same thing
    #[test]
    fn test_train_n_1_k_2_constant() {
        let observations = array![0, 0];
        let hmm = HMM::train(&observations, 1, 2, &mut new_rng());
        assert_eq!(hmm.a, array![[1.0]]);
        assert_eq!(hmm.b, array![[1.0, 0.0]]);
        assert_eq!(hmm.pi, array![1.0]);
    }

    // An HMM with one state that emits one of two things randomly
    #[test]
    fn test_train_n_1_k_2_random() {
        let observations = array![0, 1];
        let hmm = HMM::train(&observations, 1, 2, &mut new_rng());
        assert_eq!(hmm.a, array![[1.0]]);
        assert_eq!(hmm.b, array![[0.5, 0.5]]);
        assert_eq!(hmm.pi, array![1.0]);
    }

    /// This HMM should switch between its two states each time step
    #[test]
    fn test_train_n_2_k_2() {
        let observations = array![0, 1, 0];
        let hmm = HMM::train(&observations, 2, 2, &mut new_rng());

        // There are two solutions to this MLE problem
        assert_eq!(hmm.a, array![[0.0, 1.0], [1.0, 0.0]]);
        if hmm.pi == array![0.0, 1.0] {
            assert_eq!(hmm.b, array![[0.0, 1.0], [1.0, 0.0]]);
        } else {
            assert_eq!(hmm.b, array![[1.0, 0.0], [0.0, 1.0]]);
            assert_eq!(hmm.pi, array![1.0, 0.0]);
        }
    }

    #[test]
    fn test_viterbi_empty() {
        let ys = array![];
        assert_eq!(
            HMM_FANCY.most_likely_sequence(&ys),
            Array1::<usize>::zeros(0)
        );
    }

    #[test]
    fn test_viterbi_0() {
        let ys = array![0];
        assert_eq!(HMM_FANCY.most_likely_sequence(&ys), array![0]);
    }

    #[test]
    fn test_viterbi_1() {
        let ys = array![1];
        assert_eq!(HMM_FANCY.most_likely_sequence(&ys), array![2]);
    }

    #[test]
    fn test_viterbi_0_0() {
        let ys = array![0, 0];
        assert_eq!(
            HMM_FANCY.most_likely_sequence(&ys),
            most_likely_sequence_sampled(&HMM_FANCY, &ys, 1000)
        );
    }

    #[test]
    fn test_viterbi_0_1() {
        let ys = array![0, 1];
        assert_eq!(
            HMM_FANCY.most_likely_sequence(&ys),
            most_likely_sequence_sampled(&HMM_FANCY, &ys, 1000)
        );
    }

    #[test]
    fn test_viterbi() {
        let ys = array![0, 1, 0, 1];
        assert_eq!(
            HMM_FANCY.most_likely_sequence(&ys),
            most_likely_sequence_sampled(&HMM_FANCY, &ys, 10000)
        );
    }
}

#[cfg(all(test, feature = "serde-1"))]
mod serde_test {
    use crate::HMM;
    use ndarray::array;

    #[test]
    fn test_serde() {
        let hmm_unit: HMM = { HMM::new(array![[1.0]], array![[1.0]], array![1.0]) };

        let deser = serde_json::to_string(&hmm_unit).unwrap();
        let ser: HMM = serde_json::from_str(&deser).unwrap();

        assert_eq!(hmm_unit.a, ser.a);
        assert_eq!(hmm_unit.b, ser.b);
        assert_eq!(hmm_unit.pi, ser.pi);
    }
}

#[cfg(feature = "benchmark")]
mod benchmark {
    #[bench]
    fn bench(b: &mut test::Bencher) {
        use crate::{new_rng, HMM};

        let mut rng = new_rng();
        let observations = [0, 1].iter().cycle().take(1001).cloned().collect();
        b.iter(|| {
            HMM::train(&observations, 1, 2, &mut rng);
        });
    }
}
