use super::*;
use derive_getters::Getters;
use derive_new::*;
/// The item yielded by the `HMMFilterIter`.
#[derive(Clone, Debug, PartialEq, Getters,new)]
pub struct HMMFilterItem {
    p_states: Array1<f64>, // The probability that we are in each state currently
}


/// This is an iterator returned by `HMM::filter`.
#[derive(new)]
pub struct HMMFilterIter<'a, I>
where
    I: Iterator<Item = usize>,
{
    hmm: &'a HMM,
    observations: I,
    current_item: Option<HMMFilterItem>,
}

impl<'a, I> Iterator for HMMFilterIter<'a, I>
where
    I: Iterator<Item = usize>,
{
    type Item = HMMFilterItem;

    fn next(&mut self) -> Option<Self::Item> {
        self.observations.next().map(|observation| {
            let observation_probs = self.hmm.b.column(observation).to_owned();
            let transition_probs = if let Some(ref current_item) = self.current_item {
                current_item.p_states.dot(&self.hmm.a)
            } else {
                self.hmm.pi.to_owned()
            };
            let mut p_states = observation_probs * transition_probs;
            // TODO what if no state is possible, eek
            let p_states_sum: f64 = p_states.iter().sum();
            p_states /= p_states_sum;

            let item = HMMFilterItem { p_states };
            self.current_item = Some(item.clone());
            item
        })
    }
}
