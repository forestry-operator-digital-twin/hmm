#[cfg(test)]
mod tests_weighted_choice_float {
    use crate::new_rng;
    use crate::WeightedChoiceFloat;
    use rand::distributions::Distribution;

    #[test]
    fn unit() {
        let wcf = WeightedChoiceFloat::from_pmf(&[1.0]);
        assert_eq!(0, wcf.sample(&mut new_rng()))
    }

    #[test]
    fn first() {
        let wcf = WeightedChoiceFloat::from_pmf(&[1.0, 0.0]);
        assert_eq!(0, wcf.sample(&mut new_rng()))
    }

    #[test]
    fn last() {
        let wcf = WeightedChoiceFloat::from_pmf(&[0.0, 1.0]);
        assert_eq!(1, wcf.sample(&mut new_rng()))
    }

    #[test]
    fn middle() {
        let wcf = WeightedChoiceFloat::from_pmf(&[0.0, 1.0, 0.0]);
        assert_eq!(1, wcf.sample(&mut new_rng()))
    }
}